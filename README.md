```
                  
      .  _ _   _| 
 \/\/ | | (/_ (_| 
    _______________Welcome!!
```

```
Sitemap:

/ (site root)
  - boards (ex: /b)
    - threads (ex: /b/any-thread-name)
      - comments (ex: /b/any-thread-name/reference-to-any-comment)

```

### Routes -

Basically, a [CRUD scheme](https://goo.gl/Itx0lh), not mutch special :p

#### Category :: Boards

| Route                       | Method         | Function                                      | Parameters Ref    |
|:----------------------------|:---------------|:----------------------------------------------|:------------------|
| /board                      | [GET]          | View all Board`s                              | -                 |
| /board/create               | [GET]          | Call form to create a board                   | go.to.reference   |
| /board/edit/[board]         | [GET]          | Edit a existent board                         | go.to.reference   |
| /board/remove/[board]       | [GET]          | Remove a board                                | ['board']         |

Additionals:

>Loren

#### Category :: Threads

| Route                       | Method         | Function                                      | Parameters Ref    |
|:----------------------------|:---------------|:----------------------------------------------|:------------------|
| /[board]/                   | [GET]          | View all threads inside board                 | -                 |
| /[board]/create             | [GET]          | Create a new thread from board                | go.to.reference   |
| /[board]/edit/[thread-id]   | [GET]          | Edit a existent thread                        | go.to.reference   |
| /[board]/remove/[thread-id] | [GET]          | Remove a thread                               | ['thread-id']     |

Additionals:

>Loren

### Running the environment (Development mode) -

Its easy, just read the follow pre-requisites and running command: [(Read about docker)](https://goo.gl/pDYQ9A)

`docker-compose up -d`

Now, edit your local [DNS](https://goo.gl/jMfAl) file setting line `127.0.0.1 wired.dev`.

> Aka: file located on `/etc/hosts` On mac/linux or `\WINDOWS\system32\drivers\etc` on windows, If there isn't one, just create it).

Try connect on server `wired.dev`

Requirements:

    - docker
    - curl || git
