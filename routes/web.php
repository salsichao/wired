<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Board Option --
Route::get('/board/create', 'BoardController@index');
Route::post('/board/create', 'BoardController@create');

Route::get('/test', 'HomeController@index');

Route::post('/test', function () {
    die('Looks like this working - [POST]');
});

Route::get('user/{id}', function ($id) {
    return 'User '.$id;
});